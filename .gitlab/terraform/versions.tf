terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.35"
    }
  }
  required_version = ">= 1.0"
}
