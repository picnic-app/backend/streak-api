CREATE TABLE Streaks(
    ID          STRING(36) NOT NULL,
    Type        INT64  NOT NULL,
    Counter     INT64 NOT NULL,
    UserID      STRING(36) NOT NULL,
    HoursLeft   FLOAT64 NOT NULL,
    UpdatedAt   TIMESTAMP  NOT NULL OPTIONS (allow_commit_timestamp = true),
) PRIMARY KEY (ID);