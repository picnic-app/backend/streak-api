package model

import "time"

const (
	StreakType_Login = iota + 1
	StreakType_Post
	StreakType_Chat
	StreakType_Comment
)

type Streak struct {
	ID        string
	UserID    string
	Type      int64
	Counter   int64
	HoursLeft float64
	UpdatedAt time.Time `cursor:"offset,desc,default"`
}
