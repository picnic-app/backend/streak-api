package model

import "gitlab.com/picnic-app/backend/libs/golang/cursor"

type Cursor struct {
	Limit             uint32
	CursorID          string
	BackwardDirection bool
}

func (c Cursor) Params() *cursor.Params {
	dir := cursor.Forward
	if c.BackwardDirection {
		dir = cursor.Backward
	}
	return &cursor.Params{Limit: c.Limit, Dir: int(dir), ID: c.CursorID}
}
