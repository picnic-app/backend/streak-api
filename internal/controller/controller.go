package controller

import (
	"google.golang.org/grpc"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/streak-api/streak/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/service"
)

func New(svc *service.Service) Controller {
	return Controller{
		service: svc,
	}
}

// Controller represents struct that implements StreakServiceServer.
type Controller struct {
	service *service.Service
	client  eventbus.Client

	v1.UnsafeStreakServiceServer
}

func (c Controller) Register(reg grpc.ServiceRegistrar) {
	v1.RegisterStreakServiceServer(reg, c)
}
