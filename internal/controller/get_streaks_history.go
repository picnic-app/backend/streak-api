package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/streak-api/streak/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/controller/deserialize"
	"gitlab.com/picnic-app/backend/streak-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/streak-api/internal/util/slice"
)

func (c Controller) GetStreaksHistory(
	ctx context.Context,
	req *v1.GetStreaksHistoryRequest,
) (*v1.GetStreaksHistoryResponse, error) {
	streakType, err := deserialize.StreakType(req.GetType())
	if err != nil {
		return nil, err
	}

	streaks, page, err := c.service.GetStreaksHistory(ctx, streakType, deserialize.Cursor(req.GetCursor()))
	if err != nil {
		return nil, err
	}

	resp := &v1.GetStreaksHistoryResponse{
		Streaks:  slice.Convert(serialize.StreakPointer, streaks...),
		PageInfo: serialize.PageInfo(page),
	}
	return resp, nil
}
