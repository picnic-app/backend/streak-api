package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/streak-api/streak/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/controller/deserialize"
	"gitlab.com/picnic-app/backend/streak-api/internal/controller/serialize"
)

func (c Controller) CheckStreak(
	ctx context.Context,
	req *v1.CheckStreakRequest,
) (*v1.CheckStreakResponse, error) {
	streakType, err := deserialize.StreakType(req.GetType())
	if err != nil {
		return nil, err
	}

	streak, err := c.service.GetStreak(ctx, streakType)
	if err != nil {
		return nil, err
	}

	v1Streak := serialize.Streak(*streak)
	resp := &v1.CheckStreakResponse{
		Streak: v1Streak,
	}
	return resp, nil
}
