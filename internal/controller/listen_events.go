package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
)

func (c Controller) ListenEvents(ctx context.Context) {
	// Subscribe for Content events
	if err := c.client.Topic(eventbus.TopicID().Content).Sub(ctx,
		c.service.HandleUpsertStreak); err != nil {
		logger.Errorf(ctx, "error subscribing to content topic: %v", err)
	}

	// Subscrie for Circle events
	if err := c.client.Topic(eventbus.TopicID().Circle).Sub(ctx,
		c.service.HandleUpsertStreak); err != nil {
		logger.Errorf(ctx, "error subscribing to circle topic: %v", err)
	}
}
