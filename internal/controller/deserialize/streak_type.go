package deserialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/streak-api/streak/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
	"gitlab.com/picnic-app/backend/streak-api/internal/service"
)

func StreakType(t v1.StreakType) (int64, error) {
	switch t {
	case v1.StreakType_STREAK_LOGIN:
		return model.StreakType_Login, nil
	case v1.StreakType_STREAK_POST:
		return model.StreakType_Post, nil
	case v1.StreakType_STREAK_CHAT:
		return model.StreakType_Chat, nil
	case v1.StreakType_STREAK_COMMENT:
		return model.StreakType_Comment, nil
	default:
		return 0, service.ErrUnsupported("type", t)
	}
}
