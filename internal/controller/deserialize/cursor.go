package deserialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/picnic/common/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
)

func Cursor(c *v1.PageCursor) model.Cursor {
	return model.Cursor{
		Limit:             c.GetLimit(),
		CursorID:          c.GetLastId(),
		BackwardDirection: c.GetDir() == v1.PageDir_PAGE_DIR_BACKWARD,
	}
}
