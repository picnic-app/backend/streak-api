package serialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/streak-api/streak/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func Streak(s model.Streak) *v1.Streak {
	return &v1.Streak{
		Id:        s.ID,
		UserId:    s.UserID,
		Type:      v1.StreakType(s.Type),
		Counter:   s.Counter,
		HoursLeft: float32(s.HoursLeft),
		UpdatedAt: timestamppb.New(s.UpdatedAt),
	}
}

func StreakPointer(s *model.Streak) *v1.Streak {
	if s == nil {
		return nil
	}
	return Streak(*s)
}
