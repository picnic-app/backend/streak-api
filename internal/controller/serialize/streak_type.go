package serialize

import (
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/streak-api/streak/v1"
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
)

func StreakType(t int64) v1.StreakType {
	switch t {
	case model.StreakType_Login:
		return v1.StreakType_STREAK_LOGIN
	case model.StreakType_Post:
		return v1.StreakType_STREAK_POST
	case model.StreakType_Chat:
		return v1.StreakType_STREAK_CHAT
	case model.StreakType_Comment:
		return v1.StreakType_STREAK_COMMENT
	default:
		return model.StreakType_Login
	}
}
