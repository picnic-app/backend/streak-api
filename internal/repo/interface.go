package repo

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/libs/golang/cursor"
)

type ReadActions interface {
	GetStreak(context.Context, UserID, Type) ([]Streak, error)
	GetStreaksHistory(context.Context, UserID, Type, cursor.Cursor) ([]Streak, error)
}

type WriteActions interface {
	UpsertStreak(context.Context, Streak) error
}

type ReadWriteActions interface {
	ReadActions
	WriteActions
}

type Repo interface {
	SingleRead() ReadActions
	SingleWrite() WriteActions
	ReadOnlyTx(context.Context, func(ctx context.Context, tx ReadActions) error) error
	ReadWriteTx(context.Context, func(ctx context.Context, tx ReadWriteActions) error) (time.Time, error)
}
