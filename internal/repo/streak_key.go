package repo

type Streak struct {
	ID        ID
	UserID    UserID
	Type      Type
	Counter   Counter
	HoursLeft HoursLeft
	UpdatedAt UpdatedAt `cursor:"offset,desc,default"`
}
