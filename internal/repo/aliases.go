package repo

import "time"

type (
	ID        = string
	UserID    = string
	Type      = int64
	Counter   = int64
	HoursLeft = float64
	UpdatedAt = time.Time
)
