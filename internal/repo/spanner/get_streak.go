package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/Masterminds/squirrel"
	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/streak-api/internal/repo"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo/spanner/tables"
)

func (r readOnly) GetStreak(ctx context.Context, userID string, streakType int64) ([]repo.Streak, error) {
	return GetStreak(ctx, r.tx, userID, streakType)
}

func (rw readWrite) GetStreak(ctx context.Context, userID string, streakType int64) ([]repo.Streak, error) {
	return GetStreak(ctx, rw.tx, userID, streakType)
}

func GetStreak(ctx context.Context, db Queryer, userID string, streakType int64) ([]repo.Streak, error) {
	table := tables.Get().Streaks()

	q, args, err := squirrel.
		Select(table.AllColumns()...).
		From(table.TableName()).
		Where(squirrel.Eq{table.Type(): streakType, table.UserID(): userID}).
		PlaceholderFormat(squirrel.AtP).ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	params := ArgsToParams(args)

	stmt := spanner.Statement{SQL: q, Params: params}
	return GetResults[repo.Streak](db.Query(ctx, stmt))
}
