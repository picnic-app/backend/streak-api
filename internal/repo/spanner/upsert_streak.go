package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo/spanner/tables"
)

func (w writeOnly) UpsertStreak(ctx context.Context, in repo.Streak) error {
	return UpsertStreak(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), in)
}

func (rw readWrite) UpsertStreak(ctx context.Context, in repo.Streak) error {
	return UpsertStreak(ctx, rw.tx, in)
}

func UpsertStreak(_ context.Context, db BufferWriter, in repo.Streak) error {
	table := tables.Get().Streaks()
	op := spanner.InsertOrUpdate(
		table.TableName(),
		[]string{table.ID(), table.Type(), table.UserID(), table.Counter(),
			table.HoursLeft(), table.UpdatedAt()},
		[]interface{}{in.ID, in.Type, in.UserID, in.Counter, in.HoursLeft, spanner.CommitTimestamp},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
