package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"gitlab.com/picnic-app/backend/libs/golang/cursor"

	"gitlab.com/picnic-app/backend/streak-api/internal/repo"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo/spanner/tables"
)

func (r readOnly) GetStreaksHistory(
	ctx context.Context,
	userID string,
	kind int64,
	c cursor.Cursor,
) ([]repo.Streak, error) {
	return GetStreaksHistory(ctx, r.tx, userID, kind, c)
}

func (rw readWrite) GetStreaksHistory(
	ctx context.Context,
	userID string,
	kind int64,
	c cursor.Cursor,
) ([]repo.Streak, error) {
	return GetStreaksHistory(ctx, rw.tx, userID, kind, c)
}

func GetStreaksHistory(
	ctx context.Context,
	db Queryer,
	userID string,
	streakType int64,
	c cursor.Cursor,
) ([]repo.Streak, error) {
	table := tables.Get().StreaksHistory()

	q, args, err := squirrel.
		Select(table.AllColumns()...).
		From(table.TableName()).
		Where(squirrel.Eq{table.Type(): streakType, table.UserID(): userID}).
		PlaceholderFormat(squirrel.AtP).ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	params := ArgsToParams(args)
	if c != nil {
		q, params, err = cursor.GetBuilder(c, cursor.Spanner).
			WithSQL(q).
			WithParams(params).
			ToSQL()
		if err != nil {
			return nil, errors.WithStack(err)
		}
	}

	stmt := spanner.Statement{SQL: q, Params: params}
	return GetResults[repo.Streak](db.Query(ctx, stmt))
}
