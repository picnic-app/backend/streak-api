package tables

import "strings"

type Table interface {
	TableName() string
	AllColumns() []string
}

func Get() AllTables { return AllTables{} }

func WithAlias(alias string) AllTables { return AllTables{alias: alias} }

type AllTables struct{ alias string }

func aliasedTable(alias, table string) string {
	if alias == "" {
		return table
	}
	return table + " AS " + alias
}

//nolint:unused,deadcode
func withIndex(table, index string) string {
	table, alias, _ := strings.Cut(table, " AS ")
	return aliasedTable(alias, table+"@{force_index="+index+"}")
}

func aliasedCol(alias, col string) string {
	if alias == "" {
		return col
	}
	return alias + "." + col
}
