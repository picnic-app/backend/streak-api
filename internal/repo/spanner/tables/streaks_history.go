package tables

func (t AllTables) StreaksHistory() StreaksHistory { return StreaksHistory(t) }

type StreaksHistory struct{ alias string }

func (s StreaksHistory) TableAlias() string { return s.alias }
func (s StreaksHistory) TableName() string  { return aliasedTable(s.alias, "StreaksHistory") }
func (s StreaksHistory) AllColumns() []string {
	return []string{
		s.ID(),
		s.Type(),
		s.UserID(),
		s.Counter(),
		s.HoursLeft(),
		s.UpdatedAt(),
	}
}
func (s StreaksHistory) ID() string        { return aliasedCol(s.alias, "ID") }
func (s StreaksHistory) Type() string      { return aliasedCol(s.alias, "Type") }
func (s StreaksHistory) UserID() string    { return aliasedCol(s.alias, "UserID") }
func (s StreaksHistory) Counter() string   { return aliasedCol(s.alias, "Counter") }
func (s StreaksHistory) HoursLeft() string { return aliasedCol(s.alias, "HoursLeft") }
func (s StreaksHistory) UpdatedAt() string { return aliasedCol(s.alias, "UpdatedAt") }
