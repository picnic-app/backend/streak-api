package tables

func (t AllTables) Streaks() Streaks { return Streaks(t) }

type Streaks struct{ alias string }

func (s Streaks) TableAlias() string { return s.alias }
func (s Streaks) TableName() string  { return aliasedTable(s.alias, "Streaks") }
func (s Streaks) AllColumns() []string {
	return []string{
		s.ID(),
		s.Type(),
		s.UserID(),
		s.Counter(),
		s.HoursLeft(),
		s.UpdatedAt(),
	}
}
func (s Streaks) ID() string        { return aliasedCol(s.alias, "ID") }
func (s Streaks) Type() string      { return aliasedCol(s.alias, "Type") }
func (s Streaks) UserID() string    { return aliasedCol(s.alias, "UserID") }
func (s Streaks) Counter() string   { return aliasedCol(s.alias, "Counter") }
func (s Streaks) HoursLeft() string { return aliasedCol(s.alias, "HoursLeft") }
func (s Streaks) UpdatedAt() string { return aliasedCol(s.alias, "UpdatedAt") }
