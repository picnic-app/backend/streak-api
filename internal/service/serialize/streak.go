package serialize

import (
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo"
)

func Streak(s repo.Streak) model.Streak {
	return model.Streak{
		ID:        s.ID,
		UserID:    s.UserID,
		Type:      s.Type,
		Counter:   s.Counter,
		HoursLeft: float64(s.HoursLeft),
		UpdatedAt: s.UpdatedAt,
	}
}
