package service

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/picnic-app/backend/streak-api/internal/model"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo"
	"gitlab.com/picnic-app/backend/streak-api/internal/util/convert"
	"gitlab.com/picnic-app/backend/streak-api/internal/util/generator"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
)

func (s *Service) HandleUpsertStreak(ctx context.Context, e *event.Event) error {
	if e.EvtType == event.TypeNewPost {
		payload := event.NewPostPayload{}
		if err := convert.ToEvent(e.Payload, &payload); err != nil {
			logger.Errorf(ctx, "failed to convert NewPostPayload event payload: %v", err)
			return err
		}
		err := s.handleStreak(ctx, payload.UserID, model.StreakType_Post)
		if err != nil {
			logger.Errorf(ctx, "failed to UpsertStreak s.handleStreak : %v", err)
			return err
		}
	}
	if e.EvtType == event.TypeCommentNew {
		payload := event.NewCommentPayload{}
		if err := convert.ToEvent(e.Payload, &payload); err != nil {
			logger.Errorf(ctx, "failed to convert NewCommentPayload event payload: %v", err)
			return err
		}
		err := s.handleStreak(ctx, payload.UserID, model.StreakType_Comment)
		if err != nil {
			logger.Errorf(ctx, "failed to UpsertStreak s.handleStreak : %v", err)
			return err
		}
	}
	if e.EvtType == event.MessageSent {
		payload := event.MessageSentPayload{}
		if err := convert.ToEvent(e.Payload, &payload); err != nil {
			logger.Errorf(ctx, "failed to convert MessageSentPayload event payload: %v", err)
			return err
		}
		err := s.handleStreak(ctx, payload.UserID, model.StreakType_Chat)
		if err != nil {
			logger.Errorf(ctx, "failed to UpsertStreak s.handleStreak : %v", err)
			return err
		}
	}
	// TODO: add login event
	// if e.EvtType == event.TypeNewLogin {
	// 	payload := event.NewLoginPayload{}
	// 	if err := convert.ToEvent(e.Payload, &payload); err != nil {
	// 		logger.Errorf(ctx, "failed to convert CircleDiscordWebhookConfigured event payload: %v", err)
	// 		return err
	// 	}
	// 	err := s.handleStreak(ctx, payload.UserID, model.StreakType_Post)
	// 	if err != nil {
	// 		logger.Errorf(ctx, "failed to UpsertStreak s.handleStreak : %v", err)
	// 		return err
	// 	}
	// }
	fmt.Println("DONE")
	return nil
}

func (s *Service) handleStreak(ctx context.Context, userID string, streakType int64) error {
	var streak repo.Streak
	res, err := s.repo.SingleRead().GetStreak(ctx, userID, streakType)
	if err != nil {
		return err
	}

	if res == nil {
		streak = repo.Streak{
			ID:        generator.GenerateID(),
			Type:      streakType,
			UserID:    userID,
			Counter:   1,
			HoursLeft: 0,
		}
	} else {
		streak = updateStreakCounter(res[0])
	}

	// TODO : call Update coins

	if err := s.repo.SingleWrite().UpsertStreak(ctx, streak); err != nil {
		logger.Errorf(ctx, "failed to UpsertStreak event.TypeNewPost : %v", err)
		return err
	}

	return nil
}

func updateStreakCounter(streak repo.Streak) repo.Streak {
	currentTime := time.Now().In(streak.UpdatedAt.Location())
	diff := currentTime.Sub(streak.UpdatedAt)

	if diff.Hours() > 24+timeUntilMidnight(streak.UpdatedAt).Hours() {
		streak.Counter = 1
	} else if diff.Hours() > timeUntilMidnight(streak.UpdatedAt).Hours() {
		streak.Counter = streak.Counter + 1
	}
	return streak
}

func timeUntilMidnight(t time.Time) time.Duration {
	midnight := time.Date(t.Year(), t.Month(), t.Day(), 23, 59, 59, 0, t.Location())
	durationUntilMidnight := midnight.Sub(t)

	return durationUntilMidnight
}
