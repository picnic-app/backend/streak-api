package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
	"gitlab.com/picnic-app/backend/streak-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/streak-api/internal/util/is"
)

func (s *Service) GetStreak(ctx context.Context, streakType int64) (*model.Streak, error) {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	if err = s.ValidateStreakType(ctx, streakType); err != nil {
		return nil, err
	}

	streak, err := s.repo.SingleRead().GetStreak(ctx, userID, int64(streakType))
	if err != nil {
		return nil, err
	}
	res := serialize.Streak(streak[0])
	return &res, nil
}

func (s *Service) ValidateStreakType(_ context.Context, streakType int64) error {
	if is.AnyOf(streakType, model.StreakType_Login, model.StreakType_Post, model.StreakType_Chat,
		model.StreakType_Comment) {
		return nil
	}

	return ErrUnsupported("type", streakType)
}
