package service

import "gitlab.com/picnic-app/backend/streak-api/internal/repo"

func New(repo repo.Repo) *Service {
	return &Service{repo: repo}
}

type Service struct {
	repo repo.Repo
}
