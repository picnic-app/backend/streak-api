package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/cursor"
	"gitlab.com/picnic-app/backend/streak-api/internal/model"
	"gitlab.com/picnic-app/backend/streak-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/streak-api/internal/util/slice"
)

func (s *Service) GetStreaksHistory(ctx context.Context, streakType int64,
	c model.Cursor) ([]*model.Streak, cursor.Page, error) {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, nil, err
	}

	if err = s.ValidateStreakType(ctx, streakType); err != nil {
		return nil, nil, err
	}

	p := c.Params()
	if p.Limit == 0 {
		p.Limit = 5
	}

	cur, err := cursor.FromParams(model.Streak{}, p)
	if err != nil {
		return nil, nil, err
	}

	result, err := s.repo.SingleRead().GetStreaksHistory(ctx, userID, int64(streakType), cur)
	if err != nil {
		return nil, nil, err
	}

	return cursor.GetResult(cur, slice.ConvertPointers(serialize.Streak, result...))
}
