package convert

import "encoding/json"

func ToEvent[T any](in any, out *T) error {
	data, err := json.Marshal(in)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, &out)
}
