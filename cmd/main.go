package main

import (
	"context"
	"log"
	"os"
	"syscall"
	"time"

	"go.uber.org/zap/zapcore"

	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/core"
	"gitlab.com/picnic-app/backend/libs/golang/core/mw"
	"gitlab.com/picnic-app/backend/libs/golang/graceful"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/monitoring"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
	"gitlab.com/picnic-app/backend/streak-api/internal/controller"
	"gitlab.com/picnic-app/backend/streak-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/streak-api/internal/service"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	err := config.Load(ctx)
	if err != nil {
		log.Fatal(err)
	}

	initLog(ctx, config.LogLevel())

	app, err := initApp(ctx)
	if err != nil {
		log.Fatal(err)
	}
	cfg := app.Config()

	err = tracing.SetupExporter(ctx)
	if err != nil {
		logger.Errorf(ctx, "failed to set up tracing exporter: %v", err)
	}

	monitoring.RegisterPrometheusSuffix()

	debugSrv, err := app.RunDebug(ctx)
	if err != nil {
		logger.Fatalf(ctx, "failed to start debug server: %v", err)
	}

	ctrl, cleanup := initController(ctx, cfg)

	gracefulShutdown := graceful.New(
		&graceful.ShutdownManagerOptions{Timeout: 60 * time.Second},
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "servers",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				app.Close()
				return nil
			}),
			graceful.HTTPServer(debugSrv),
		),
		graceful.Context(cancel),
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "clients",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(cleanup),
			graceful.Tracer(),
		),
		graceful.Logger(nil),
	)
	gracefulShutdown.RegisterSignals(os.Interrupt, syscall.SIGTERM)
	defer func() {
		_ = gracefulShutdown.Shutdown(context.Background())
	}()

	err = app.Run(ctx, ctrl)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	logger.Info(ctx, "gRPC server closed gracefully")
}

func initLog(ctx context.Context, logLevelCfg string) {
	logLevel, err := zapcore.ParseLevel(logLevelCfg)
	if err != nil {
		log.Fatal(ctx, err)
	}

	logger.SetLevel(logLevel)
}

func initApp(ctx context.Context) (core.Application, error) {
	app, err := core.InitAppWithOptions(ctx, mw.LogOptions{
		RedactRequestFunc:  func(a any) any { return a },
		RedactResponseFunc: func(a any) any { return a },
	})
	if err != nil {
		return nil, err
	}

	app = app.WithMW(mw.NewServerContextInterceptor(config.String("env.auth.secret")))

	return app, nil
}

func initController(ctx context.Context, cfg config.Config) (controller.Controller, func() error) {
	db, err := spanner.NewSpannerClient(ctx, cfg.Spanner.DSN, cfg.Spanner.ADCPath)
	if err != nil {
		logger.Fatalf(ctx, "failed to create spanner client: %v", err)
	}

	repo := spanner.NewRepoWithClient(db)

	svc := service.New(repo)

	return controller.New(svc), func() error {
		db.Close()
		return nil
	}
}
